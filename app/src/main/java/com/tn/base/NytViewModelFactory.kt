package com.tn.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

/**
 * Created by QAIT\amarkhatri.
 */
@Singleton
class NytViewModelFactory @Inject constructor(private val providerMap: @JvmSuppressWildcards Map<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        /*if (modelClass !is BaseViewModel)
            throw IllegalArgumentException("Model class $modelClass must be instance of BaseViewModel class")*/
      /* return when (modelClass) {
            DashboardViewModel::class -> providerMap.get(modelClass)
            else -> throw RuntimeException("Unsupported viewModel class")
        }*/
        var provider = providerMap[modelClass]
        if(provider==null){
            for((key,value) in providerMap){
                if(modelClass.isAssignableFrom(key)){
                    provider=value
                    break
                }
            }
        }
        if(provider==null)
            throw IllegalArgumentException("Unknown model class $modelClass")

        return provider.get() as T
    }
}