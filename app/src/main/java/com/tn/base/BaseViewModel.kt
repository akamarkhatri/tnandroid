package com.tn.base

import androidx.lifecycle.ViewModel
import com.tn.data.repository.AppRepository

/**
 * Created by QAIT\amarkhatri.
 */
abstract class BaseViewModel constructor(val appRepository: AppRepository):ViewModel(){
    val taskStatusLiveData by lazy {
        appRepository.taskStatusLiveData
    }
}