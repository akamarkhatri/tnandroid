package com.tn.utils

/**
 * Created by QAIT\amarkhatri on 14/4/18.
 */

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

private val BACKGROUND_EXECUTOR = Executors.newFixedThreadPool(2)
val IO_EXECUTOR: ExecutorService = Executors.newSingleThreadExecutor()
private val mainThreadHandler by lazy {
    Handler(Looper.getMainLooper())
}
private val MAIN_THREAD_EXECUTOR by lazy {
    Executor {
        mainThreadHandler.post(it)
    }
}

/**
 * Utility method to run blocks on a dedicated background thread, used for io/database work.
 */
fun ioThread(f: () -> Unit) {
    IO_EXECUTOR.execute(f)
}

/**
 * Utility method to run blocks on a main thread.
 */
fun mainThread(f: () -> Unit) {
    MAIN_THREAD_EXECUTOR.execute(f)
}
fun backgroundThread(f:()->Unit){
    BACKGROUND_EXECUTOR.execute(f)
}