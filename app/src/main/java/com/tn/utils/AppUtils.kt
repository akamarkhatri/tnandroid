package com.tn.utils

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.tn.R

/**
 * Created by QAIT\amarkhatri.
 */
private val LOG_TAG = "NYT->"

fun notifyUserWithLog(msg: String,tag: String = "") {
    Log.d(LOG_TAG + tag, msg)
}
fun AppCompatActivity.showToast(msg: String) {
    android.widget.Toast.makeText(this, msg, android.widget.Toast.LENGTH_SHORT).show()
}
fun AppCompatActivity.showSnackbar(view: View?, msg: String, actionValue: String = "") {
    view ?: return showToast(msg)
    Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show()
    if (android.text.TextUtils.isEmpty(actionValue)) return
    Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE).setAction(actionValue, android.view.View.OnClickListener {

    }).show()
}
fun Context.isConnectedToNetwork(): Boolean {
    val connectivityManager: ConnectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetworkInfo = connectivityManager.activeNetworkInfo

    return activeNetworkInfo?.isConnected ?: false
}

fun AppCompatActivity.showCheckYourInternetMsg() {
    showToast(getString(R.string.msg_check_internet))
}
fun ImageView.loadThumbnailImage(imageUrl: String?, placeHolder_id: Int = R.drawable.default_article_icom) {
    imageUrl?:return
    Glide.with(this)
        .asBitmap()
        .apply(RequestOptions.placeholderOf(placeHolder_id))
        .load(imageUrl ?: "")
        .thumbnail(0.1f)
        .into(this)
}