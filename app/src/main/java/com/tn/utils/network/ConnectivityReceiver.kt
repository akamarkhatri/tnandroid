package com.tn.utils.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tn.utils.isConnectedToNetwork

/**
 * Created by QAIT\amarkhatri.
 */
class ConnectivityReceiver: BroadcastReceiver() {
        val connectionStatusLiveData: LiveData<Boolean> by lazy {
            MutableLiveData<Boolean>()
    }
    override fun onReceive(context: Context?, intent: Intent?) {
        val connectionStatus = context?.isConnectedToNetwork() ?: false
        val prevStatus = connectionStatusLiveData.value
        if(prevStatus ==connectionStatus)
            return
        if(connectionStatus && prevStatus==null)
            return
        (connectionStatusLiveData as MutableLiveData)
                .postValue(context?.isConnectedToNetwork()?:false)
    }
}
enum class ConnectionStatus{
    CONNECTED,
    DISCONNECTED
}
interface ConnectionStatusCallback{
    fun onConnected()
    fun onDisconnected()
}