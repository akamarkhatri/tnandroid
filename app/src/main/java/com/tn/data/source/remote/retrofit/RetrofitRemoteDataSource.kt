package com.tn.data.source.remote.retrofit

import com.tn.data.Article
import com.tn.data.source.remote.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by QAIT\amarkhatri.
 */
class RetrofitRemoteDataSource(apiKey:String):RemoteDataSource(apiKey) {
    override fun getArticleList(
        articleListRequest: ArticleListRequest,
        remoteDataSourceCallback: RemoteDataSourceCallback<List<Article>>
    ) {
        apiService.getArticles(articleListRequest.period,
            apiKey).enqueue(object : Callback<ArticleListResponse>{
            override fun onFailure(call: Call<ArticleListResponse>, t: Throwable) {
                remoteDataSourceCallback.onError(getErrorMsg(t))
            }

            override fun onResponse(call: Call<ArticleListResponse>, response: Response<ArticleListResponse>) {
                val list = response.body()?.results ?: return onFailure(
                    call,
                    ServerResponseError(SERVER_ERR_SOME_THING_ELSE_WENT_WRONG)
                )
                remoteDataSourceCallback.onSuccess(list)
            }

        })
    }

    private val apiService: ApiService by lazy {
        ApiClient.getClient().create(ApiService::class.java)
    }

}