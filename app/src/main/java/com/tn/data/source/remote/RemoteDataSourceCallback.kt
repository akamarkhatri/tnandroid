package com.tn.data.source.remote



/**
 * Created by QAIT\amarkhatri.
 */
interface RemoteDataSourceCallback<T> {
    fun onError(msg:String)
    fun onSuccess(t: T)
}