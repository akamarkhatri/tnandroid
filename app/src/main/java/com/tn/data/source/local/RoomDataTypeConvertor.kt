package com.tn.data.source.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tn.data.MediaDetail

/**
 * Created by QAIT\amarkhatri.
 */
class RoomDataTypeConvertor {
    @TypeConverter
    fun convertMediaDetailListToString(list: ArrayList<MediaDetail>?):String?{
        list?:return null
        val type=object : TypeToken<ArrayList<MediaDetail>>(){}.type
        return Gson().toJson(list,type)
    }
    @TypeConverter
    fun convertStringToMediaDetailList(value:String?):ArrayList<MediaDetail>?{
        value?:return null
        val type=object : TypeToken<ArrayList<MediaDetail>>(){}.type
        return Gson().fromJson(value,type)
    }
    @TypeConverter
    fun convertStringListToString(list: ArrayList<String>?):String?{
        list?:return null
        val type=object : TypeToken<ArrayList<String>>(){}.type
        return Gson().toJson(list,type)
    }
    @TypeConverter
    fun convertStringToStringList(value:String?):ArrayList<String>?{
        value?:return null
        val type=object : TypeToken<ArrayList<String>>(){}.type
        return Gson().fromJson(value,type)
    }
    /*@TypeConverter
    fun convertMediaDetailListToString(mediaDetail: ArrayList<MediaDetail>):String?{
        mediaDetail?:return null
        val type=object : TypeToken<MediaDetail>(){}.type
        return Gson().toJson(mediaDetail,type)
    }*/
    /*@TypeConverter
    fun convertStringToMediaDetail(value:String?):MediaDetail?{
        value?:return null
        val type=object : TypeToken<MediaDetail>(){}.type
        return Gson().fromJson(value,type)
    }*/
}