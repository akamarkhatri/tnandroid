package com.tn.data.source.local

import android.app.Application
import androidx.paging.DataSource
import com.tn.data.Article
import javax.inject.Inject

/**
 * Created by QAIT\amarkhatri.
 */
interface LocalDataSource{
    fun getArticlePagedListDataSource(): DataSource.Factory<Int, Article>
    fun saveArticleInDb(t: List<Article>)
}