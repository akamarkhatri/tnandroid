package com.tn.data.source.remote

/**
 * Created by QAIT\amarkhatri.
 */
data class ArticleListRequest(val period:Int)