package com.tn.data.source.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.tn.R
import com.tn.data.Article

/**
 * Created by QAIT\amarkhatri.
 */
@Database(entities = [Article::class],
    version = 3)
@TypeConverters(RoomDataTypeConvertor::class)
abstract class AppDatabase : RoomDatabase() {
    companion object{
        @Volatile
        private var appDatabase:AppDatabase?=null
        @Volatile
        private var appDatabaseInMemory:AppDatabase?=null

        fun getAppDatabase(context: Context):AppDatabase{
            synchronized(this){
                if(appDatabase==null){
                    appDatabase = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java,
                        context.getString(R.string.app_name))
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return appDatabase!!
        }
        fun getAppDatabaseInmemory(context: Context):AppDatabase{
            synchronized(this) {
                if (appDatabaseInMemory == null) {
                    appDatabaseInMemory = Room.inMemoryDatabaseBuilder(context.applicationContext,
                        AppDatabase::class.java)
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                    return appDatabaseInMemory!!
                }
            }
            return appDatabaseInMemory!!
        }

    }
    abstract fun getArticleDetailDao():ArticleDetailDao
}