package com.tn.data.source.remote

import com.tn.data.Article

/**
 * Created by QAIT\amarkhatri.
 */
abstract class RemoteDataSource(val apiKey:String) {
    abstract fun getArticleList(articleListRequest: ArticleListRequest,remoteDataSourceCallback: RemoteDataSourceCallback<List<Article>>)
}