package com.tn.data.source.remote.retrofit

import com.tn.data.Article
import com.tn.data.source.remote.ArticleListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


/**
 * Created by QAIT\amarkhatri.
 */
interface ApiService {
    @GET("mostpopular/v2/viewed/{period}.json")
    fun getArticles(@Path("period") period:Int,
                    @Query("api-key")api_key:String): Call<ArticleListResponse>
}