package com.tn.data.source.remote

import com.tn.data.Article

/**
 * Created by QAIT\amarkhatri.
 */
data class ArticleListResponse(val results:ArrayList<Article>?)