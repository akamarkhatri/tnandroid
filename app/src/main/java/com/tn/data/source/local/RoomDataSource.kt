package com.tn.data.source.local

import android.app.Application
import androidx.paging.DataSource
import com.tn.data.Article
import javax.inject.Inject

/**
 * Created by QAIT\amarkhatri.
 */
class RoomDataSource (application: Application):LocalDataSource {
    override fun getArticlePagedListDataSource(): DataSource.Factory<Int, Article> {
        return appDb.getArticleDetailDao().getAllArticleDataSource()
    }

    override fun saveArticleInDb(t: List<Article>) {
        appDb.getArticleDetailDao().insert(t)
    }

    private val inMemoryDB by lazy {
        AppDatabase.getAppDatabaseInmemory(application)

    }
    private val appDb by lazy {
        AppDatabase.getAppDatabase(application)
    }
}