package com.tn.data.source.remote

/**
 * Created by QAIT\amarkhatri.
 */
data class ServerResponseError(val msg:String) : Exception(msg)