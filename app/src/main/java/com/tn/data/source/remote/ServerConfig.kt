package com.tn.data.source.remote

import android.util.MalformedJsonException
import com.google.gson.JsonSyntaxException
import java.net.ConnectException
import java.net.UnknownHostException

/**
 * Created by QAIT\amarkhatri.
 */
private const val NETWORK_ERR = "Please check your internet connection and try again."

private const val SERVER_CONNECTION_ERROR = "Failed to connect to the server, please try after some time."

private const val SERVER_ERROR_JSON_EXCEPTION = "Server response is invalid."

private const val SERVER_TIMEOUT_ERROR = "Server is taking too much time to load."

 const val SERVER_ERR = "Server error, please try again after some time."
const val EMPTY_RESPONSE = "Response is empty"
const val SERVER_ERR_SOME_THING_ELSE_WENT_WRONG= "Something went wrong. Try again"
const val PRIME_OFFLINE_DAY_LIMIT=4

fun getErrorMsg(error: Throwable?): String {
    if (error == null) {
        return SERVER_TIMEOUT_ERROR
    }
    return when {
        error is UnknownHostException -> NETWORK_ERR
        error is ConnectException -> SERVER_CONNECTION_ERROR
        error is NullResponseError -> SERVER_ERR
        error is ServerResponseError -> error.msg
        error is IllegalStateException || error is JsonSyntaxException || error is MalformedJsonException -> SERVER_ERROR_JSON_EXCEPTION
        else -> SERVER_TIMEOUT_ERROR
    }

}

private const val BASE_URL = "https://api.nytimes.com/svc/"
fun getBaseUrl(): String {
    return BASE_URL
}


