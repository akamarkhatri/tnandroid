package com.tn.data.source.remote

/**
 * Created by QAIT\amarkhatri.
 */
data class ServerResponse(val data: Any)