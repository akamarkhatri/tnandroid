package com.tn.data.source.local

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tn.data.Article

/**
 * Created by QAIT\amarkhatri.
 */
@Dao
interface ArticleDetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<Article>)

    @Query("select * from Article")
    fun getAllArticleDataSource(): DataSource.Factory<Int,Article>

    @Query("select count(*) from Article")
    fun getArticleCount():Int

    @Query("delete from Article")
    fun deleteAll()
}