package com.tn.data.source.remote.retrofit

import com.tn.BuildConfig
import com.tn.data.source.remote.getBaseUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by QAIT\amarkhatri.
 */
class ApiClient {
    companion object {
        private val retrofit: Retrofit = initClient()

        fun getClient(): Retrofit {
            return retrofit
        }

        private fun initClient(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(getBaseUrl())
//                    .baseUrl(dummyBaseUrl)
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        private fun getHttpClient(): OkHttpClient {

            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpLoggingInterceptor.level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(3, TimeUnit.MINUTES)
                .addInterceptor(httpLoggingInterceptor)
            return okHttpClient.build()
        }


    }

}

data class PostReqBody(
    val encodedNames: List<String>?,
    val encodedValues: List<String>?
)