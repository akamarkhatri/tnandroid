package com.tn.data

import com.google.gson.annotations.SerializedName

/**
 * Created by QAIT\amarkhatri.
 */
data class MediaDetail(var type:String?,
                       var subtype:String?,
                       var caption:String?,
                       var copyright:String?,
                       var approved_for_syndication:Int?,
                       @SerializedName("media-metadata")
                        var media_metadata:ArrayList<MediaMetadata>?
                       ) {
    fun getThumbnail(): String? {
        media_metadata?:return null
        media_metadata?.forEach {
            return it.url
        }
        return null
    }
}

data class MediaMetadata(var url:String?,
                         var format:String?,
                         var height:Int?,
                         var width:Int?)