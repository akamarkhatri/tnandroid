package com.tn.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.tn.dashboard.ArticlePeriod
import com.tn.data.Article
import com.tn.data.Task
import com.tn.data.TaskState
import com.tn.data.TaskStatus
import com.tn.data.source.local.LocalDataSource
import com.tn.data.source.remote.ArticleListRequest
import com.tn.data.source.remote.RemoteDataSource
import com.tn.data.source.remote.RemoteDataSourceCallback
import com.tn.utils.ioThread
import javax.inject.Inject

/**
 * Created by QAIT\amarkhatri.
 */
class AppRepository @Inject constructor(
    private val localDataSource: LocalDataSource,
    private val remoteDataSource: RemoteDataSource) {
    val taskStatusLiveData: LiveData<TaskStatus> by lazy {
        MutableLiveData<TaskStatus>()
    }

    private fun changeTaskStatus(taskStatus: TaskStatus) {
        (taskStatusLiveData as MutableLiveData)
            .postValue(taskStatus)
    }
    fun getArticlePagedListDataSource(): DataSource.Factory<Int, Article> {
        return localDataSource.getArticlePagedListDataSource()
    }

    fun fetchArticle(articlePeriod: ArticlePeriod, refreshing: Boolean=false) {
        val taskId=Task.FETCH_ARTICLES
        if(refreshing)
            TaskStatus.refreshing(taskId)
        else
            TaskStatus.loading(taskId)

        changeTaskStatus(TaskStatus.loading(taskId))
        remoteDataSource.getArticleList(articleListRequest = ArticleListRequest(articlePeriod.period),
            remoteDataSourceCallback = object :RemoteDataSourceCallback<List<Article>>{
                override fun onError(msg: String) {
                    changeTaskStatus(TaskStatus.error(taskId,msg))
                }

                override fun onSuccess(t: List<Article>) {
                    ioThread {
                        localDataSource.saveArticleInDb(t)
                        changeTaskStatus(TaskStatus.loaded(taskId))
                    }
                }

            })
    }
}