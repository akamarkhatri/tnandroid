package com.tn.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.tn.data.source.local.RoomDataTypeConvertor

/**
 * Created by QAIT\amarkhatri.
 */
@Entity
data class Article(
    @PrimaryKey
    var id: Long?=null,
    var url: String?=null,
    var adx_keywords: String?="",
    var title: String?="",
    var abstract: String?=null,
    var published_date: String?=null,
    var byline: String?=null,
    var views: Long?=null,
    @TypeConverters(RoomDataTypeConvertor::class)
    var media: ArrayList<MediaDetail>?=null
){
    fun getThumbnail():String?{
        media?:return null
        media?.forEach {
            if(it.type==MediaType.IMAGE.typeVal){
                return it.getThumbnail()
            }
        }
        return null
    }
}
enum class MediaType(val typeVal:String){
    IMAGE("image")
}
/*,
    @TypeConverters(RoomDataTypeConvertor::class)
    var des_facet: ArrayList<String>?=null,
    @TypeConverters(RoomDataTypeConvertor::class)
    var org_facet: ArrayList<String>?=null,
    @TypeConverters(RoomDataTypeConvertor::class)
    var per_facet: ArrayList<String>?=null,
    @TypeConverters(RoomDataTypeConvertor::class)
    var geo_facet: ArrayList<String>?=null,

*/
