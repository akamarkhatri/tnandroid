package com.tn.data

class TaskStatus private constructor(
    val taskState: TaskState,
    val taskId: Task, val response: Any? = null, val msg: String = ""
) {
    companion object {
        fun loaded(taskId: Task, response: Any? = null, msg: String = "") =
            TaskStatus(TaskState.LOADED, taskId, response, msg)

        fun loading(taskId: Task) = TaskStatus(TaskState.LOADING, taskId)
        fun error(taskId: Task, msg: String = "") = TaskStatus(TaskState.FAILED, taskId, msg = msg)
        fun refreshing(taskId: Task) = TaskStatus(TaskState.REFRESHING, taskId)
    }

}

enum class TaskState {
    FAILED,
    LOADING,
    LOADED,
    REFRESHING
}

enum class Task {
    FETCH_ARTICLES
}