package com.tn.dashboard

import android.os.Bundle
import android.view.Menu
import androidx.fragment.app.Fragment
import com.tn.R
import com.tn.base.BaseActivity
import kotlinx.android.synthetic.main.activity_dashboard.*

/**
 * Created by QAIT\amarkhatri.
 */
class ActivityDashboard():BaseActivity() {
    companion object{
        const val TAG_ARTICLE_FRAGMENT="RepoFragment"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setupView()
    }

    private fun setupView() {
        setSupportActionBar(toolbar)
        if(getFragment()!=null)
            return
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container,
                FragmentArticleList(), TAG_ARTICLE_FRAGMENT).commit()
    }
    private fun getFragment(): Fragment? {
        return supportFragmentManager.findFragmentByTag(TAG_ARTICLE_FRAGMENT)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_dashboard,menu)
        return super.onCreateOptionsMenu(menu)
    }
}