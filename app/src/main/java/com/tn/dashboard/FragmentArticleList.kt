package com.tn.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gojk.githubrepo.home.ItemSelectedCallback
import com.tn.R
import com.tn.base.BaseFragment
import com.tn.data.Article
import com.tn.data.TaskState
import com.tn.utils.isConnectedToNetwork
import com.tn.utils.notifyUserWithLog
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.fragment_article_list.*
import kotlinx.android.synthetic.main.shimmer_progress_repo_list.*
import javax.inject.Inject

/**
 * Created by QAIT\amarkhatri.
 */
class FragmentArticleList: BaseFragment() {
    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory
    /*private val articleListViewModel by lazy {
        ViewModelProviders.of(appCompatActivity).get(ArticleListiewModel::class.java)
    }*/
    private val articleListViewModel by lazy {
        ViewModelProviders.of(appCompatActivity,viewModelProviderFactory).get(ArticleListiewModel::class.java)
    }
    private val itemSelectedCallback :ItemSelectedCallback<Article> by lazy {
        object :ItemSelectedCallback<Article>{
            override fun onItemSelected(t: Article) {

            }

        }
    }
    private val articlePagedListAdapter by lazy {
        ArticlePagedListAdapter(itemSelectedCallback)
    }
    override fun getFragmentView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(
            R.layout.fragment_article_list,
            container,
            false
        )
    }
    lateinit var period: ArticlePeriod
    override fun setUpView(view: View?, savedInstanceState: Bundle?) {
        /*later initialize with bundle*/
        period=ArticlePeriod.PERIOD_7
        observeTaskStatusLivedata()
        article_recycler_view.layoutManager = LinearLayoutManager(
            appCompatActivity,
            RecyclerView.VERTICAL, false
        )
        article_recycler_view.adapter=articlePagedListAdapter
        articleListViewModel.getArticlePagedListLivedata(period)
            .observe(this, Observer {
                articlePagedListAdapter.submitList(it)
            })

        swipe_refresh.setOnRefreshListener {
            if (!appCompatActivity.isConnectedToNetwork()) {
                swipe_refresh.isRefreshing = false
                return@setOnRefreshListener
            }
            articleListViewModel.fetchRepo(period,isRefreshing = true)
        }
        setupErrorView()
    }

    private fun observeTaskStatusLivedata() {
        articleListViewModel.taskStatusLiveData
            .observe(appCompatActivity, Observer {
                it ?: return@Observer
                notifyUserWithLog(it.taskState.name)
                when (it.taskState) {
                    TaskState.FAILED -> {
                        if (swipe_refresh.isRefreshing) {
                            swipe_refresh.isRefreshing = false
                            return@Observer
                        }
                        hideProgress()
                        showError()
                    }
                    TaskState.LOADING -> {
                        showProgress()
                    }
                    TaskState.LOADED -> {
                        if (swipe_refresh.isRefreshing) {
                            swipe_refresh.isRefreshing = false
//                            return@Observer
                        }
                        hideError()
                        hideProgress()
                    }
                    TaskState.REFRESHING -> {

                    }
                }
            })
    }
    private fun setupErrorView() {
        action_retry.setOnClickListener {
            if(appCompatActivity.isConnectedToNetwork())
                articleListViewModel.fetchRepo(period)
        }
    }

    override fun showProgress() {
        hideError()
        shimmer_progress.visibility = View.VISIBLE
        shimmerContainer.startShimmer()
        swipe_refresh.visibility = View.GONE
    }

    override fun hideProgress() {
        swipe_refresh.visibility = View.VISIBLE
        shimmerContainer.stopShimmer()
        shimmer_progress.visibility = View.GONE
    }

    override fun showError(msg: String) {
        error_layout.visibility=View.VISIBLE
        swipe_refresh.visibility=View.GONE
    }

    override fun hideError() {
        error_layout.visibility=View.GONE
    }
}