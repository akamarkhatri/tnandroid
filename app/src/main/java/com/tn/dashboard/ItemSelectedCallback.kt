package com.gojk.githubrepo.home

import com.tn.data.Article

/**
 * Created by QAIT\amarkhatri.
 */
interface ItemSelectedCallback<T> {
    fun onItemSelected(t: T)
}