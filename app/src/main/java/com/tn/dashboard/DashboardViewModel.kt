package com.tn.dashboard

import com.tn.base.BaseViewModel
import com.tn.data.repository.AppRepository

/**
 * Created by QAIT\amarkhatri.
 */
class DashboardViewModel(appRepository: AppRepository) : BaseViewModel(appRepository)