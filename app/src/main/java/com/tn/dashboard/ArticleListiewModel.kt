package com.tn.dashboard

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.tn.base.BaseViewModel
import com.tn.data.Article
import com.tn.data.repository.AppRepository
import javax.inject.Inject

/**
 * Created by QAIT\amarkhatri.
 */
class ArticleListiewModel @Inject constructor(appRepository: AppRepository):BaseViewModel(appRepository) {
    private val pageListConfig by lazy {
        PagedList.Config.Builder()
            .setPageSize(10)
            .setPrefetchDistance(10)
            .setEnablePlaceholders(false)
            .build()
    }
    fun getArticlePagedListLivedata(articlePeriod: ArticlePeriod): LiveData<PagedList<Article>> {
        val articlePagedListDataSource = appRepository.getArticlePagedListDataSource()
        val liveData = LivePagedListBuilder<Int, Article>(articlePagedListDataSource, pageListConfig)
            .setBoundaryCallback(ArticlePagedListBoundaryCallback(appRepository, articlePeriod))
            .build()

        return liveData
    }

    fun fetchRepo(period: ArticlePeriod,
                  isRefreshing:Boolean=false) {
        appRepository.fetchArticle(period,isRefreshing)
    }
}