package com.tn.dashboard

import androidx.paging.PagedList
import com.tn.data.Article
import com.tn.data.repository.AppRepository

/**
 * Created by QAIT\amarkhatri.
 */
class ArticlePagedListBoundaryCallback(val appRepository: AppRepository,
                                       val articlePeriod:ArticlePeriod):PagedList.BoundaryCallback<Article>() {
    private var apiCalledForZeroItems: Boolean = false

    override fun onZeroItemsLoaded() {
        if(apiCalledForZeroItems)
            return
        apiCalledForZeroItems=true
        appRepository.fetchArticle(articlePeriod)
    }

    override fun onItemAtEndLoaded(itemAtEnd: Article) {
        super.onItemAtEndLoaded(itemAtEnd)
    }

    override fun onItemAtFrontLoaded(itemAtFront: Article) {
        super.onItemAtFrontLoaded(itemAtFront)
    }
}