package com.tn.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.gojk.githubrepo.home.ItemSelectedCallback
import com.tn.R
import com.tn.base.BaseViewHolder
import com.tn.data.Article
import com.tn.utils.loadThumbnailImage
import kotlinx.android.synthetic.main.article_list_content_item.view.*

/**
 * Created by QAIT\amarkhatri.
 */
class ArticlePagedListAdapter(val itemSelectedCallback: ItemSelectedCallback<Article>) : PagedListAdapter<Article, BaseViewHolder>(
    DIFF_CALLBACK
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val itemLayout = LayoutInflater.from(parent.context).inflate(R.layout.article_list_content_item, parent, false)
        return BaseViewHolder(itemLayout)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val article = getItem(position)
        holder.itemView.title.text=article?.title?:""
        holder.itemView.written_by.text=article?.byline?:""
        holder.itemView.date.text=article?.published_date?:""
        holder.itemView.action_open_article.setOnClickListener {
            article?:return@setOnClickListener
            itemSelectedCallback.onItemSelected(article)
        }
        holder.itemView.setOnClickListener {
            article?:return@setOnClickListener
            itemSelectedCallback.onItemSelected(article)
        }
        holder.itemView.article_thumbnail.loadThumbnailImage(article?.getThumbnail())

    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Article>() {
            override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
                return oldItem == oldItem
            }

        }
    }
}