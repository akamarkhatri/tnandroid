package com.tn.dashboard

/**
 * Created by QAIT\amarkhatri.
 */
enum class ArticlePeriod(val period:Int) {
    PERIOD_7(7),
    PERIOD_15(15),
    PERIOD_30(30),

}