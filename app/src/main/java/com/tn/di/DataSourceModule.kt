package com.tn.di

import android.app.Application
import com.tn.R
import com.tn.data.source.local.LocalDataSource
import com.tn.data.source.local.RoomDataSource
import com.tn.data.source.remote.RemoteDataSource
import com.tn.data.source.remote.retrofit.RetrofitRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by QAIT\amarkhatri.
 */
@Module
class DataSourceModule {

    @Provides
    fun provideLocalDataSource(application: Application):LocalDataSource{
        return RoomDataSource(application)
    }
    @Provides
    fun provideRemoteDataSource(application: Application):RemoteDataSource{
        return RetrofitRemoteDataSource(application.getString(R.string.api_key))
    }
}