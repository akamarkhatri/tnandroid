package com.tn.di

import android.app.Application
import com.tn.NytApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by QAIT\amarkhatri.
 */
@Singleton
@Component(modules = [AndroidInjectionModule::class,
    DataSourceModule::class,
    ViewModelModule::class,
    ArticleListModule::class])
interface AppComponent :AndroidInjector<NytApplication>{

    @Component.Factory
    interface Factory{
        // With @BindsInstance, the Context passed in will be available in the graph
        fun create(@BindsInstance context: Application): AppComponent

    }

}