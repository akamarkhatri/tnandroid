package com.tn.di

import dagger.Module
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tn.base.BaseViewModel
import com.tn.base.NytViewModelFactory
import com.tn.dashboard.ArticleListiewModel
import com.tn.dashboard.DashboardViewModel
import com.tn.data.repository.AppRepository
import dagger.Binds
import dagger.MapKey
import kotlin.reflect.KClass
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Provider


/**
 * Created by QAIT\amarkhatri.
 */
@Module
abstract class ViewModelModule {


    @Binds
    internal abstract fun bindViewModelFactory(
        factory: NytViewModelFactory
    ): ViewModelProvider.Factory

}
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)