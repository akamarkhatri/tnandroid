package com.tn.di

import androidx.lifecycle.ViewModel
import com.tn.base.BaseViewModel
import com.tn.dashboard.ArticleListiewModel
import com.tn.dashboard.DashboardViewModel
import com.tn.dashboard.FragmentArticleList
import com.tn.data.repository.AppRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 * Created by QAIT\amarkhatri.
 */
@Module
abstract class ArticleListModule {
    @ContributesAndroidInjector
    abstract fun articleListFragment(): FragmentArticleList

    @Binds
    @IntoMap
    @ViewModelKey(ArticleListiewModel::class)
    abstract fun bindViewModel(viewmodel: ArticleListiewModel): ViewModel
}