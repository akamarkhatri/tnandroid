package com.tn.di

import android.app.Application
import android.content.Context
import com.tn.data.repository.AppRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by QAIT\amarkhatri.
 */
@Module
abstract class AppModule {
    @Binds
    abstract fun bindRepository(repository: AppRepository):AppRepository
}