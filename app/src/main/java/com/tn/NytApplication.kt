package com.tn

import android.app.Application
import com.tn.di.AppComponent
import com.tn.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * Created by QAIT\amarkhatri.
 */
open class NytApplication: DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
//        return appComponent
        return DaggerAppComponent.factory().create(this)
    }

    /*val appComponent by lazy {
        initializeComponent()
    }
    open fun initializeComponent():AppComponent{
        return DaggerAppComponent.factory().create(this)
    }*/

}